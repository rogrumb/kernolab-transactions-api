<?php


namespace Tests\Unit;

use App\Enums\TransactionStatuses;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StoreTransactionTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_success()
    {
        $transactionData = Transaction::factory()->definition();

        $response = $this->postJson($this->getRoute(), $transactionData);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => $this->getTransactionSchema()
            ]);
    }

    public function test_failed()
    {
        $transactionData = Transaction::factory()->definition();

        $response = $this->postJson(
            $this->getRoute(),
            array_merge($transactionData, ['user_id' => 'asd'])
        );

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => ['user_id']
            ]);
    }

    private function getRoute(): string
    {
        return route('api.transactions.store');
    }
}
