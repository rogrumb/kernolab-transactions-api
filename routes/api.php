<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api.'], function () {
    Route::group(['prefix' => 'transactions', 'as' => 'transactions.'], function () {
        Route::post('/', [TransactionsController::class, 'store'])->name('store');
        Route::post('submit', [TransactionsController::class, 'submit'])->name('submit');
        Route::get('{transaction}', [TransactionsController::class, 'show'])->name('show');
    });
});
