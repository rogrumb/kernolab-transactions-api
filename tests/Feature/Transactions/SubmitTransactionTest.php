<?php


namespace Tests\Unit;

use App\Enums\TransactionStatuses;
use App\Models\Transaction;
use App\Services\BasicTwoFactorAuthenticator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubmitTransactionTest extends TestCase
{
    use RefreshDatabase;

    public function test_success()
    {
        $transaction = Transaction::factory()->create();

        $transactionSubmitInfo = [
            'code' => BasicTwoFactorAuthenticator::DEFAULT_AUTH_CODE,
            'transaction_id' => $transaction->id
        ];

        $response = $this->postJson($this->getRoute(), $transactionSubmitInfo);

        $response->assertStatus(200)
            ->assertJson([
                'status' => 'Your transaction is being submitted'
            ]);
    }

    public function test_failed_with_wrong_code()
    {
        $transaction = Transaction::factory()->create();

        $transactionSubmitInfo = [
            'code' => '222',
            'transaction_id' => $transaction->id
        ];

        $response = $this->postJson($this->getRoute(), $transactionSubmitInfo);

        $response->assertStatus(400)
            ->assertJsonStructure(['status']);
    }

    public function test_failed_with_wrong_transaction_id()
    {
        $transaction = Transaction::factory()->create();

        $transactionSubmitInfo = [
            'code' => BasicTwoFactorAuthenticator::DEFAULT_AUTH_CODE,
            'transaction_id' => $transaction->id . rand()
        ];

        $response = $this->postJson($this->getRoute(), $transactionSubmitInfo);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => ['transaction_id']
            ]);
    }

    private function getRoute(): string
    {
        return route('api.transactions.submit');
    }
}
