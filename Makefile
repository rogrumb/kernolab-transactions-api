init:  docker-up app-start
start: docker-up
restart: docker-down docker-up
down: docker-down
test: app-tests
test-coverage: app-test-coverage
migrate: app-migrate
rollback: app-rollback

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

app-migrate:
	docker-compose exec php-fpm php artisan migrate --force

app-rollback:
	docker-compose exec php-fpm php artisan migrate:rollback

app-start:
	docker-compose exec php-fpm composer install
	docker-compose exec php-fpm php artisan key:generate
	docker-compose exec php-fpm php artisan cache:clear
	docker-compose exec php-fpm php artisan migrate
	docker-compose exec php-fpm php artisan storage:link
	docker-compose exec php-fpm php artisan db:seed
	docker-compose restart

app-tests:
	docker-compose exec php-fpm php artisan test --testsuite=Feature --stop-on-failure

app-test-coverage:
	docker-compose exec php-fpm vendor/bin/phpunit --testsuite=Feature --coverage-html reports/

bash:
	docker-compose exec php-fpm bash

permissions:
	sudo chown -R $$USER:www-data storage
	sudo chown -R $$USER:www-data bootstrap/cache
	chmod -R 775 storage
	chmod -R 775 bootstrap/cache
	chmod -R 777 storage/logs/

cache:
	docker-compose exec  php-fpm php artisan cache:clear
	docker-compose exec  php-fpm php artisan view:clear
	docker-compose exec  php-fpm php artisan route:clear
	docker-compose exec  php-fpm php artisan config:cache
