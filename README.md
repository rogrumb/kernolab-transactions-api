# Transactions Api

Transactions api task solution

## Getting started

In order to start the application you need to:

1. Clone repository
2. Create .env file as a copy of .env.example(for testing speed puproses everything is there and ready to work)
3. `make init`
4. server is available at `localhost:8010`

## Useful commands

1. stop containers: `make down`
2. test: `make test`
3. test-coverage: `make test-coverage`
4. run: `make start`
5. you can find out some more useful commands in make file

## Important Notes

Technologies used: docker, laravel, mysql, redis, php-fpm.

I intentionally skipped some Laravel features to reduce complexity of the whole project.

I left a lot of comments in the code that can give you some information about decisions that I made.

### Here is a quick summary:

* architecture: I used service oriented architecture to reduce controllers and models complexity in scope of MVC pattern.
I also implemented some design patterns that I love to use e.g. dependency injection. I did not implement too many of them
because it will significantly increase the complexity of this tiny project. There a lot of places where you can easily improve
performance of beauty of the specific procedure or entity, sometimes I mentioned it, sometimes not.

* tech req changes: transaction_id is added to submit request

* tests: tests are written in a really fast way just to show up how those could be done initially. I used factories with faker 
to create some mock data for being able to test the functionality of some feature along the project. Also, I used named routes
and some test's helper methods and traits such as asserJson(Structure) or DatabaseRefresh. I haven't added any test environment
because I don't think it's point in that very task.


If you have any questions or suggestions please contact me!

p.s. I decided to leave all laravel initial files to keep the structure consistent and durable, cause laravel highly relies on its dependencies. 
