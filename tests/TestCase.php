<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    private array $transactionSchema = [
        'transaction_id',
        'user_id',
        'details',
        'receiver_account',
        'receiver_name',
        'amount',
        'currency',
        'fee',
        'status',
    ];

    /**
     * @return string[]
     */
    public function getTransactionSchema(): array
    {
        return $this->transactionSchema;
    }
}
